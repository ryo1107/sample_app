Rails.application.routes.draw do
  get '/signup', to: 'users#new'
  # get '/home', to: 'stastic_pages#home'
  get '/help', to: 'stastic_pages#help'
  get '/about', to: 'stastic_pages#about'
  get '/contact', to: 'stastic_pages#contact'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "stastic_pages#home"
end
