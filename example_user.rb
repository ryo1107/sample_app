class User
  attr_accessor :name,:email

  def initialize(attribues={})
    @fast_name = attribues[:fast_name]
    @last_name = attribues[:last_name]
    @email = attribues[:email]
  end

  def full_name
    "#{@fast_name} #{@last_name}"
  end

  def alphabetical_name
    "#{@fast_name},#{@last_name}"
  end

  def formatted_email
    "#{@fast_name} #{@last_name}<#{@email}>"
  end
end
